import React from 'react';
import {
  View,
  Text,
  StatusBar,
  SafeAreaView,
  TouchableWithoutFeedback,
  Keyboard,
  Dimensions,
} from 'react-native';
import {Button, Input, Header} from '../../components';
import {FlatList} from 'react-native-gesture-handler';

const {width, height} = Dimensions.get('window');
class EnquiryScreen extends React.Component {
  render() {
    return (
      <>
        <StatusBar
          barStyle="dark-content"
          backgroundColor="white"
          translucent={false}
        />
        <SafeAreaView style={{flex: 0, backgroundColor: 'white'}} />
        <SafeAreaView style={{flex: 1}}>
          <Header
            rightIcon={true}
            text="Enquiry Details"
            onPressR={() => this.props.navigation.navigate('addenquiry')}
          />
        </SafeAreaView>
      </>
    );
  }
}
EnquiryScreen.navigationOptions = {
  headerShown: false,
};
export default EnquiryScreen;
