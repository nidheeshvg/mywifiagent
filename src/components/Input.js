import React from 'react';
import {View, Text, TouchableOpacity, Image, TextInput} from 'react-native';
const Input = ({
  text,
  placeholder,
  onEndEditing,
  RightIcon = false,
  height = 50,
  multiline = false,
}) => {
  return (
    <View
      style={{
        flex: 1,
      }}>
      <Text
        allowFontScaling={false}
        style={{
          color: 'rgb(73,58,195)',
          fontSize: 13,
          fontWeight: '700',
        }}>
        {text}
      </Text>
      <View
        style={{
          borderColor: 'gray',
          borderWidth: 0.5,
          flexDirection: 'row',
          marginTop: 4,
          height: height,
          //alignItems: 'center',
          borderRadius: 2,
        }}>
        <Image
          style={{width: 25, height: 25, marginLeft: 10, marginTop: 10}}
          source={{
            uri:
              'https://cdn.iconscout.com/icon/premium/png-256-thumb/mobile-1681-724698.png',
          }}
        />
        <TextInput
          multiline={multiline}
          style={{marginLeft: 10, flex: 1, alignSelf: 'baseline'}}
          placeholder="sdfghjk"
        />
        {RightIcon == true ? (
          <Image
            style={{width: 25, height: 25, marginRight: 10, marginTop: 10}}
            source={{
              uri:
                'https://cdn.iconscout.com/icon/premium/png-256-thumb/mobile-1681-724698.png',
            }}
          />
        ) : null}
      </View>
    </View>
  );
};
export {Input};
