import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
//import Icon from 'react-native-vector-icons/dist/FontAwesome';Feather
const Header = ({onPressL, onPressR, text, rightIcon = false}) => {
  return (
    <View style={{height: 60}}>
      <LinearGradient
        colors={['rgb(73,58,195)', 'rgb(118,36,164)']}
        start={{x: 0, y: 0}}
        end={{x: 0, y: 2}}
        style={{
          flex: 1,
          alignItems: 'center',
          //justifyContent: 'center',
          flexDirection: 'row',
        }}>
        <TouchableOpacity style={{marginLeft: 10, width: 50, height: 30}}>
          <Ionicons name="arrow-back" color="white" size={30} />
          {/* <Icon name="rocket" size={30} color="#900" /> */}
        </TouchableOpacity>
        <Text
          allowFontScaling={false}
          style={{flex: 1, color: 'white', fontSize: 20, textAlign: 'center'}}>
          {text}
        </Text>
        <TouchableOpacity
          style={{marginLeft: 10, width: 50, height: 30}}
          onPress={onPressR}>
          {rightIcon == true ? (
            <Feather name="plus" color="white" size={30} />
          ) : null}
        </TouchableOpacity>
      </LinearGradient>
    </View>
  );
};
export {Header};
