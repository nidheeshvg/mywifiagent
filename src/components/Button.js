import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const Button = ({onPress, ButtonText}) => {
  return (
    <TouchableOpacity style={{flex: 1}} onPress={onPress}>
      <LinearGradient
        colors={['rgb(73,58,195)', 'rgb(118,36,164)']}
        start={{x: 0, y: 0}}
        end={{x: 0, y: 2}}
        style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text
          allowFontScaling={false}
          style={{color: 'white', fontSize: 19, fontWeight: '900'}}>
          {ButtonText}
        </Text>
      </LinearGradient>
    </TouchableOpacity>
  );
};
export {Button};
