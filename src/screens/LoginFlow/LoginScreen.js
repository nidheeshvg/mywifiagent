import React from 'react';
import {
  View,
  Text,
  StatusBar,
  SafeAreaView,
  TouchableWithoutFeedback,
  Keyboard,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {Button, Input} from '../../components';

const {width, height} = Dimensions.get('window');
class LoginScreen extends React.Component {
  render() {
    return (
      <>
        <StatusBar
          barStyle="dark-content"
          backgroundColor="white"
          translucent={false}
        />
        <SafeAreaView style={{flex: 0, backgroundColor: 'white'}} />

        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <SafeAreaView style={{flex: 1}}>
            <View style={{flex: 1}}>
              <View style={{flex: 1, backgroundColor: 'gray'}}></View>
              <View style={{flex: 1}}>
                <Text
                  allowFontScaling={false}
                  style={{
                    color: 'rgb(73,58,195)',
                    fontSize: 22,
                    alignSelf: 'center',
                  }}>
                  Login or Register
                </Text>
                <View style={{height: 70, margin: 20}}>
                  <Input />
                </View>
                <View style={{height: 70, margin: 20, marginTop: 5}}>
                  <Input />
                </View>
                <Text
                  allowFontScaling={false}
                  style={{
                    color: 'gray',
                    fontSize: 11,
                    marginHorizontal: 40,
                    alignSelf: 'center',
                    textAlign: 'center',
                  }}>
                  Create password with last 4 digits of mobile number + first
                  two letters of the contact person's name
                </Text>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('register')}>
                  <Text
                    style={{
                      textAlign: 'center',
                      marginTop: 20,
                      color: 'rgb(73,58,195)',
                    }}>
                    new user? register here
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{flex: 1}}></View>
            </View>
            <View style={{height: 60}}>
              <Button
                ButtonText="REGISTER"
                onPress={() => this.props.navigation.navigate('register')}
              />
            </View>
          </SafeAreaView>
        </TouchableWithoutFeedback>
      </>
    );
  }
}
export default LoginScreen;
