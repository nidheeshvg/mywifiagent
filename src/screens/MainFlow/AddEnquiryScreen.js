import React from 'react';
import {
  View,
  Text,
  StatusBar,
  SafeAreaView,
  TouchableWithoutFeedback,
  Keyboard,
  Dimensions,
  Image,
} from 'react-native';
import {Button, Input, Header} from '../../components';
import {FlatList} from 'react-native-gesture-handler';

const {width, height} = Dimensions.get('window');
const products = [
  {
    id: '1',
    name: 'WIFI',
    image:
      'https://cdn.iconscout.com/icon/premium/png-256-thumb/mobile-1681-724698.png',
  },
  {
    id: '2',
    name: 'WIFI',
    image:
      'https://cdn.iconscout.com/icon/premium/png-256-thumb/mobile-1681-724698.png',
  },
  {
    id: '3',
    name: 'WIFI',
    image:
      'https://cdn.iconscout.com/icon/premium/png-256-thumb/mobile-1681-724698.png',
  },
];
class AddEnquiryScreen extends React.Component {
  render() {
    return (
      <>
        <StatusBar
          barStyle="dark-content"
          backgroundColor="white"
          translucent={false}
        />
        <SafeAreaView style={{flex: 0, backgroundColor: 'white'}} />
        <SafeAreaView style={{flex: 1}}>
          <Header text="Add An Enquiry" />
          <View style={{flex: 1}}>
            <View style={{height: 70, margin: 20}}>
              <Input text="Customer Name" />
            </View>
            <View style={{height: 70, margin: 20}}>
              <Input text="Mobile Number" />
            </View>
            <Text
              allowFontScaling={false}
              style={{
                color: 'rgb(73,58,195)',
                fontSize: 13,
                fontWeight: '700',
                marginLeft: 20,
                marginTop: 10,
              }}>
              Product Required
            </Text>
            <FlatList
              data={products}
              keyExtractor={(obj) => obj.id}
              numColumns={2}
              style={{marginHorizontal: (width - 280) / 2}}
              renderItem={({item}) => {
                return (
                  <View
                    style={{
                      margin: 20,
                      width: 100,
                      height: 120,
                    }}>
                    <View
                      style={{
                        borderColor: 'gray',
                        borderWidth: 1,
                        width: 90,
                        height: 90,
                        alignSelf: 'center',
                      }}>
                      <TouchableWithoutFeedback style={{flex: 1}}>
                        <Image
                          style={{width: 80, height: 80, alignSelf: 'center'}}
                          source={{
                            uri:
                              'https://cdn.iconscout.com/icon/premium/png-256-thumb/mobile-1681-724698.png',
                          }}
                        />
                      </TouchableWithoutFeedback>
                    </View>
                    <Text
                      allowFontScaling={false}
                      style={{
                        color: 'rgb(73,58,195)',
                        fontSize: 13,
                        fontWeight: '700',
                        alignSelf: 'center',
                        marginTop: 5,
                      }}>
                      {item.name}
                    </Text>
                  </View>
                );
              }}
            />
          </View>
          <View style={{height: 60}}>
            <Button
              ButtonText="SAVE"
              //onPress={() => this.props.navigation.navigate('MainFlow')}
            />
          </View>
        </SafeAreaView>
      </>
    );
  }
}
AddEnquiryScreen.navigationOptions = {
  headerShown: false,
};
export default AddEnquiryScreen;
