initialState = {
  showLoader: false,
  language: 'en',
  errorMsg: '',
  UserName: '',
  OtpVerified: false,
  isBusinessAccount: false,
  isNewUser: false,
  purpose: '',
  OtpSendingNumber: null,
  userData: {
    alternateNumber: '',
    emailID: '',
    firstName: '',
    lastName: '',
    mobileNumber: '',
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'addLanguageSignin':
      return {
        ...state,
        language: action.payload,
      };
    case 'SignInSucess':
      return {
        ...state,
        errorMsg: '',
        UserName: action.payload.details.Name,
        showLoader: false,
      };
    case 'Show_errorMsg':
      return {...state, errorMsg: action.payload};

    case 'OtpVerified':
      return {...state, OtpVerified: action.payload};
    case 'ShowLoader':
      return {...state, showLoader: true};
    case 'clear_errorMsg':
      return {...state, errorMsg: ''};
    case 'HideLoader':
      return {...state, showLoader: false};
    case 'AddOtpNumber':
      return {...state, OtpSendingNumber: action.payload};
    case 'newUser':
      return {...state, isNewUser: true};
    case 'add_purpose':
      return {...state, purpose: action.payload};
    case 'add_userData':
      return {...state, userData: action.payload};
    default:
      return state;
  }
};
