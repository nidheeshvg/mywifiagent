import React from 'react';
import {
  View,
  Text,
  StatusBar,
  SafeAreaView,
  TouchableWithoutFeedback,
  Keyboard,
  Dimensions,
} from 'react-native';
import {Button, Input, Header} from '../../components';
import DropDownPicker from 'react-native-dropdown-picker';
import Icon from 'react-native-vector-icons/Ionicons';
const {width, height} = Dimensions.get('window');
class RegisterScreen extends React.Component {
  state = {
    countries: ['uk'],
  };
  render() {
    return (
      <>
        <StatusBar
          barStyle="dark-content"
          backgroundColor="white"
          translucent={false}
        />
        <SafeAreaView style={{flex: 0, backgroundColor: 'white'}} />

        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <SafeAreaView style={{flex: 1}}>
            <View style={{flex: 1}}>
              <View>
                <Header text="Register" />
              </View>
              <View style={{flex: 1}}>
                <View style={{height: 70, margin: 20}}>
                  <Input text="Office or Shope Name" />
                </View>
                <View style={{height: 100, margin: 20, marginTop: 5}}>
                  <Input text="Address" height={100} multiline={true} />
                </View>
                <View style={{height: 70, margin: 20, marginTop: 15}}>
                  <Input text="Location Captured" RightIcon={true} />
                </View>
                <View style={{height: 70, margin: 20, marginTop: 15}}>
                  <Text
                    allowFontScaling={false}
                    style={{
                      color: 'rgb(73,58,195)',
                      fontSize: 13,
                      fontWeight: '700',
                    }}>
                    Contact Person
                  </Text>
                  <DropDownPicker
                    items={[
                      {
                        label: 'UK',
                        value: 'uk',
                        icon: () => <Icon name="flag" size={18} color="#900" />,
                      },
                      {
                        label: 'France',
                        value: 'france',
                        icon: () => <Icon name="flag" size={18} color="#900" />,
                      },
                    ]}
                    defaultValue={this.state.country}
                    containerStyle={{height: 50, marginTop: 10}}
                    style={{backgroundColor: '#fafafa'}}
                    placeholder="Select person"
                    //placeholderStyle={{color: 'gray'}}
                    itemStyle={{
                      justifyContent: 'flex-start',
                    }}
                    dropDownStyle={{backgroundColor: '#fafafa'}}
                    onChangeItem={(item) =>
                      this.setState({
                        country: item.value,
                      })
                    }
                  />
                </View>
                <View style={{height: 100, margin: 20, marginTop: 5}}>
                  <Input text="Mobile Number" />
                </View>
              </View>
            </View>
            <View style={{height: 60}}>
              <Button
                ButtonText="SAVE"
                onPress={() => this.props.navigation.navigate('MainFlow')}
              />
            </View>
          </SafeAreaView>
        </TouchableWithoutFeedback>
      </>
    );
  }
}
export default RegisterScreen;
