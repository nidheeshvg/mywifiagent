/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import AppNavigator from './AppNavigator';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import reducers from '../src/store/reducers';
import ReduxThunk from 'redux-thunk';
import {setNavigator} from './navigationRef';
const App = () => {
  return (
    <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))}>
      <AppNavigator
        ref={(navigator) => {
          setNavigator(navigator);
        }}
      />
    </Provider>
  );
};
export default App;
