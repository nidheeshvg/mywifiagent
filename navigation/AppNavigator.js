import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import LoginScreen from '../src/screens/LoginFlow/LoginScreen';
import RegisterScreen from '../src/screens/LoginFlow/RegisterScreen';
import EnquiryScreen from '../src/screens/MainFlow/EnquiryScreen';
import AddEnquiryScreen from '../src/screens/MainFlow/AddEnquiryScreen';

const loginFlow = createSwitchNavigator({
  login: {screen: LoginScreen},
  register: {screen: RegisterScreen},
});
const mainFlow = createStackNavigator({
  enquiry: {screen: EnquiryScreen},
  addenquiry: {screen: AddEnquiryScreen},
});
const App = createSwitchNavigator({
  LoginFlow: loginFlow,
  MainFlow: mainFlow,
});
export default createAppContainer(App);
