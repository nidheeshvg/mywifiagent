import {combineReducers} from 'redux';
import SigninReducer from './SigninReducer';

export default combineReducers({
  SignIn: SigninReducer,
});
