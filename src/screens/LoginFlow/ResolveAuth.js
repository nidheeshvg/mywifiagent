import React from 'react';
import {
  View,
  Text,
  StatusBar,
  SafeAreaView,
  TouchableWithoutFeedback,
} from 'react-native';
class ResolveAuth extends React.Component {
  render() {
    return (
      <>
        <StatusBar
          barStyle="dark-content"
          backgroundColor="white"
          translucent={false}
        />
        <SafeAreaView style={{flex: 0, backgroundColor: 'white'}} />

        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <SafeAreaView style={{flex: 1}}></SafeAreaView>
        </TouchableWithoutFeedback>
      </>
    );
  }
}
export default ResolveAuth;
